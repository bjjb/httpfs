express = require 'express'
path = require 'path'
morgan = require 'morgan'
{name, version} = require '../package'

app = express()
app.use morgan('combined')

port = process.env.HTTPFS_PORT or process.env.PORT or 8080
root = process.env.HTTPFS_ROOT or process.env.ROOT or '.'

app.use express.static(path.join(__dirname, 'www'))

start = (root = root, port = port) ->
  app.set 'root', root
  app.listen port, ->
    {name, version} = require '../package'
    {address, port} = @address()
    console.log "%s v%s listening on %s:%s", name, version, address, port

module.exports = {start}
