{name, version} = require '../package'
cli = require 'commander'
cli.version version

serve = cli.command 'serve [dir]'
serve.description 'start serving a directory'
serve.option '-p, --port <n>', 'listen on port n'
serve.action (dir = '.') ->
  server = require './server'
  server.start(dir, port)

ls = cli.command 'ls [path]'
ls.description 'list files'
ls.option '-l, --long', 'print more detail'
ls.action (path = '/') ->
  checkRemote()
  remote().ls(ls.long)

get = cli.command 'get <url>'
get.description 'get a file over http'
get.option '-o, --output <path>', 
module.exports = cli
