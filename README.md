# HTTPFS

HTTP exposure to a part of the filesystem.

This is _not_ a complete file-system, nor is it intended to replace WebDav or
equivalent technologies. It exists purely to allow one service to access files
over HTTP using an intiutive REST API. I find it useful, for example, for
hosting dynamic content on Heroku - HTTPFS is running on a cheap VPN with some
disk space, and the app's frontend on Heroku is configured to read and write
assets from that server.

## Installation

To install the server:

    npm install httpfs

If you only need to use the client from within a page, you can use the
following HTML snippet:

```
<script src='//bjjb.github.io/httpfs.js'></script>
```

## Usage

### Server

Start the server with `httpfsd`, or `npm start` from within the project
directory. It uses the following environment variables:

* `HTTPFS_ROOT` - the path to serve (default: the current directory)

To use the client in the browser, for example, to manage an image gallery:
```
<script>
  var httpfs = new HTTPFS('https://username:password@example.com')
  httpfs.get('/path/to/files, function(response) {
    if (resp
  })
</script>
```

Replace `myapikey` with 

## Usage


