httpfs = require '../lib/httpfs'

describe "httpfs", ->
  it "is available", ->
    expect("httpfs").to.be.a 'function'
